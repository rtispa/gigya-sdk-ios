Pod::Spec.new do |spec|
    spec.name       = "gigya-sdk"
    spec.version    = "3.7.4"
    spec.summary    = "The iOS client library provides an Objective-C interface for the Gigya API"
    spec.homepage   = "http://developers.gigya.com/display/GD/iOS"
    spec.author     = "denis.mi@gigya-inc.com"
    spec.license    = { :type => "Copyright", :text => <<-LICENSE
                            Copyright 2017 Gigya. See the terms of service at http://www.gigya.com/terms-of-service
                        LICENSE
                    }
    spec.platform   = :ios, "8.0"
    spec.ios.vendored_frameworks  = "GigyaSDK.framework" 
    spec.source     = { :git => "https://bitbucket.org/rtispa/gigya-sdk-ios.git", :tag => "v#{spec.version}"  }
end
