# GigyaSDK

We just add [./GigyaSDK.framework/Modules/module.modulemap](./GigyaSDK.framework/Modules/module.modulemap) in order to use GigyaSDK in our swift framework.

The file [./GigyaSDK.framework/Modules/module.modulemap](./GigyaSDK.framework/Modules/module.modulemap) contains this code:

```
framework module GigyaSDK {
  umbrella header "Gigya.h"

  export *
  module * { export * }

}
```

Could you please include [module.modulemap](./GigyaSDK.framework/Modules/module.modulemap) in your next release?

